from .base import *
import aioredis
import aiopg

async def setup_redis(bot):
    bot.redis = await aioredis.create_redis_pool('redis://localhost')

async def setup_postgres_pool(bot):
    bot.pg_pool = await aiopg.create_pool(POSTGRES_AUTH)

DEBUG = True

TOKEN = ''

BOT_PREFIX = ("b?", "b!")

GUILD_ID = 1

POSTGRES_AUTH = ''

COGS = (
    'plugins.meme',
    'plugins.notifications',
    'plugins.loaders',
    'plugins.messages',
    'plugins.lmgtfy',
    'plugins.utilities',
    'plugins.embed',
    'plugins.event_logger',
    'plugins.deleter',
)

IN_NICK = [
]

WORRY_CHECK = [
]
